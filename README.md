# HWFDD - Hello World from Dockerized Debian

Demonstrates how to build an immutable image based on the `debian:bullseye-slim` tag valid today (`Digest:sha256:8dd559eed3086238a27a6cad15ec5c4b11c0868840d363a0a12cb72405ed3c21`):

![screen-shot from docker hub debian:bullseye-slim on 2022-02-17 09:07 CET](screenshot.png)

Usage:

    docker build . -t hello-debian
    docker run --rm -it hello-debian
